﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using raico.Models;

namespace raico
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
             services.AddMvc();
             services.AddDbContext<TiendaContext>(o=> o.UseNpgsql("Server=ec2-174-129-252-252.compute-1.amazonaws.com;Username=qbadxvzdsljjoi;Password=1dd7e724cf9a1f4f68b029b48b140241f79f23eb6b40e7a32212c9e342b4fa05;Database=da99vauejq4olm;Port=5432;SSL Mode=Require;Trust Server Certificate=true"));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseMvcWithDefaultRoute();  
            app.UseStaticFiles();
           
        }
    }
}
