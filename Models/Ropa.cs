using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace raico.Models
{
    public class Ropa
    {
         public int Id { get; set; }

        public string Nombre { get; set; }
        
        public decimal Precio { get; set; }

        public string Foto { get; set; }

        public Categoria Categoria { get; set; }

        public Tipos Tipos { get; set; }
        
        public int? CategoriaId { get; set; }

        public int? TiposId { get; set; }
     

        
    }
}