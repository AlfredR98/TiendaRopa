

using Microsoft.EntityFrameworkCore;


namespace raico.Models
{
    public class TiendaContext : DbContext
    {
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Ropa> Ropas { get; set; }

        public TiendaContext(DbContextOptions<TiendaContext> o) : base(o){}
    
    }
}