using System.Collections.Generic;

namespace raico.Models
{
    public class Tipos
    {
        public int Id { get; set; }

        public string Nombre { get; set; }

        public List<Ropa> Ropas { get; set; }
    }
}