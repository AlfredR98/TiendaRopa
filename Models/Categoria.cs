 using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace raico.Models
{
    public class Categoria
    {
 
        public int Id { get; set; }

        public string Nombre { get; set; }

        public List<Ropa> Ropas { get; set; }

    }

}