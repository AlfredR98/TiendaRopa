﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using raico.Models;

namespace raico.Controllers
{
    public class HomeController : Controller
    {
         private TiendaContext _context;

        public HomeController(TiendaContext x){
            _context=x;
        }

        public IActionResult Index()
        {   
            return View();
        }
        public IActionResult Iniciar(){
            return View();
        }

        public IActionResult Nosotros(){
            return View();
        }
        public IActionResult Insertar(){
            return View();
        }
         [HttpPost]
        public IActionResult Iniciar(string Correo, string Contraseña){
           var cliente = _context.Usuarios.FirstOrDefault(e=> e.Correo == Correo && e.Contraseña == Contraseña);

            if(cliente!=null){

                 if(cliente.Contraseña=="admin"){
                        return RedirectToAction("Admi");
                    }else{
                        return RedirectToAction("Index");
                    }
               
            }   return RedirectToAction("Iniciar");
        }

        public IActionResult Admi(){
            return View();
        }

         public IActionResult Registrar(){
            return View();
        }

        [HttpPost]
        public IActionResult Registrar(Usuario u){

         if(ModelState.IsValid){
                _context.Add(u);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(u);
        }  




        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
