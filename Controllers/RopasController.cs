using Microsoft.AspNetCore.Mvc;

namespace raico.Models{

    public class RopasController : Controller{
         public IActionResult Index(){
            return View();
        }
        public IActionResult Iniciar(){
            return View();
        }

        public IActionResult Registrar(){
            return View();
        }

        public IActionResult Hombre(){
            return View();
        }

        public IActionResult Mujer(){
            return View();
        }

         public IActionResult ChompaH(){
            return View();
        }
         public IActionResult PantalonH(){
            return View();
        }
         public IActionResult PoloH(){
            return View();
        }
         public IActionResult ZapatillaH(){
            return View();
        }

         public IActionResult ChompaM(){
            return View();
        }
         public IActionResult PantalonM(){
            return View();
        }
         public IActionResult PoloM(){
            return View();
        }
         public IActionResult ZapatillaM(){
            return View();
        }
    }
}